# QC-Map-Var Workflow
This pipeline executes quality control, mapping and variant calling in one line. 
> Version: 1.0.0 <br>
> Contact: BI-Support@rki.de<br>
> Documentation updated: 31.07.2017 <br>

## Introduction

The QC-Mapping-VC workflow executes all three pipelines, [QCumber version 2](https://gitlab.com/RKIBioinformaticsPipelines/QCumber/tree/version_2), [Mapping version 4](https://gitlab.com/RKIBioinformaticsPipelines/Mapping) and [Variant Calling version 3](https://gitlab.com/RKIBioinformaticsPipelines/VariantCalling) pipeline in one commandline. In the first step, QCumber trims reads using --trimBetter and pipes the output to the mapping pipeline (mapping step in QCumber will be skipped). All reads will be mapped to the reference(s) using our in-house developed mapping pipeline. 


The pipeline uses the following tools:

| Tool name | Version | Pubmed ID | 
|-----------|---------|-----------|
| [snakemake](https://bitbucket.org/snakemake/snakemake/wiki/Home) | 3.12.0 ||
| [FastQC](https://www.bioinformatics.babraham.ac.uk/projects/fastqc/)    | 0.11.5  ||
| [Trimmomatic](http://www.usadellab.org/cms/?page=trimmomatic) | 0.36 | [24695404](https://www.ncbi.nlm.nih.gov/pubmed/24695404)|
| [Bowtie2](http://bowtie-bio.sourceforge.net/bowtie2/index.shtml)   | 2.2.9   | [22388286](https://www.ncbi.nlm.nih.gov/pubmed/22388286) |
| [Kraken](http://ccb.jhu.edu/software/kraken/) | 0.10.5 |[24580807](https://www.ncbi.nlm.nih.gov/pubmed/24580807)
| [Bowtie2*](http://bowtie-bio.sourceforge.net/bowtie2/index.shtml) | 2.3  |  [22388286](https://www.ncbi.nlm.nih.gov/pubmed/22388286) |
| [BWA*](http://bio-bwa.sourceforge.net/) | 0.7.15 | [20080505](https://www.ncbi.nlm.nih.gov/pubmed/20080505)  |
| [Latex](https://www.latex-project.org/) | 2.6 |
| [Samtools](http://www.htslib.org/)| 1.3.1 |[19505943](https://www.ncbi.nlm.nih.gov/pubmed/19505943) |
| RefFinder | 2.1.2 | [Stephan Fuchs, FG13](https://portal.rki.local/cocoon/portal/portallink?doctype=Navknoten&id=1340) |
| [Picard](https://broadinstitute.github.io/picard/)  | 2.7.1 | |
| [GATK](https://www.broadinstitute.org/gatk/download/)| 3.7 | [20644199](https://www.ncbi.nlm.nih.gov/pubmed/20644199) |
| [snpEff](http://snpeff.sourceforge.net/)   | 4.2 | [22728672](https://www.ncbi.nlm.nih.gov/pubmed/22728672) |

The workflow used in the pipeline is visualized in the following chart:

![Workflow](workflow.png "Workflow image")


## Tutorial

Calling the pipeline with the option `-h` provides a help message listing all options:

```sh
qc_map_var-1 -h
```

A more detailed description of the most important parameters is given in the section "Functions".

A small example dataset is provided in the data-folder /pipelines/datasets/benchmarking/qc_map_var. The following example uses this dataset to demonstrate a basic run of the pipeline.

A basic pipeline run is as follows:

```sh
qc_map_var-1 --input /pipelines/datasets/benchmarking/qc_map_var/data/ --reference /pipelines/datasets/benchmarking/qc_map_var/references/reference.gb --output workflow_output 
```

To perform batch analysis use `--input` and pass the complete folder containing NGS reads (reads in subfolder allowed). With this standard commandline, quality control and read trimming is performed with `--trimBetter mapping`. The reference should be given as GenBank format.  As default, the variant calling pipeline will only annotate coding regions. To enable the annotation of non-coding region, set `--vc_annotate_all`.


For repeating analysis with the same parameters, you can write default parameters in yaml format and pass it using the option `--config`. The structure of **config.yaml** is very easy. All input parameters can be listed in the format `<parameter_name> : <parameter>`. An example is provided in /pipelines/stable/qc_map_var/1/src/template.yaml

```sh
qc_map_var-1 --input /pipelines/datasets/benchmarking/qc_map_var/data/ --reference /pipelines/datasets/benchmarking/qc_map_var/references/reference.gb --output workflow_output --config /pipelines/stable/qc_map_var/1/src/template.yaml
```

If you add additional parameters, it overrides the values in the config file.


## Functions

The following paragraphs describe the most important parameters in detail.
#### Help
> Long option: `--help ` <br>
> Short option: `-h`

show this help message and exit

#### Version
> Long option: `--version `<br>
> Short option: `-v`

show program's version number and exit

#### Output
> Long option: `--output OUTPUT`<br>
> Short option: `-o` <br>
> default: working directory

Output directory

#### Config
> Long option: `--config CONFIG`<br>
> Short option: `-c`

Define a config file in yaml format with preset parameters. If both config file and command parameters are given, the parameters given on the commandline supersede the parameters given in the config file. Thus, a config file can be used to personalize default parameters, and deviations from these personal defaults can be given via the command-line.

For instance, in order to run the pipeline with 24 threads using `"~/SCRATCH_NOBAK/runs/newest_run"` as input, `"~/SCRATCH_NOBAK/runs/newest_qmvres"` as output and `"~/references/myreference.gb"` as reference, the following config file could be used (`"~/example.yaml"`):

```
threads: 24
input: "~/SCRATCH_NOBAK/runs/newest_run"
output: "~/SCRATCH_NOBAK/runs/newest_qmvres"
reference: "~/references/myreference.gb"
```

The pipeline could then be started using the following command:

```
qc_map_var-1 --config ~/example.yaml
```

If then a a run of the pipeline is to be started with the same parameters but with 12 threads, this can be achieved with the following command (re-using the config file and overriding the threads option instead of giving all paramenters on the commandline manually):

```
qc_map_var-1 --config ~/example.yaml --threads 12
```


#### Threads
> Long option: `--threads THREADS`<br>
> Short option: `-t`

Number of threads to use

#### Log
> Long option: `--log LOG`<br>
> Short option: `-l`

log file or directory

#### Input
> Long option: `--input INPUT`<br>
> Short option: `-i`

Input sample folder. Illumina filenames should end with _number, e.g. Sample_12_345_R1_001.fastq, to find the right paired set. If this does not match, all files are treated as single end data. This is always the case for IonTorrent data, i.e. if the input file is in bam-format.

#### Read1
> Long option: `--read1 READ1`<br>
> Short option: `-1`

First read of read-pair

#### Read2
> Long option: `--read2 READ2`<br>
> Short option: `-2`

Second read of read-pair

#### Reference
> Long option: `--reference REFERENCE`<br>
> Short option: `-r`

Path to GenBank file that is used as reference sequence. This is a required parameter.
GenBank files may be acquired from NCBI. When downloading GenBank files from NCBI, make sure to include the sequence (Customize view -> Display options -> Show sequence) before downloading the file (Send -> Complete record -> File -> GenBank (full)). Alternatively, the reference sequence can be saved as GenBank file from Geneious.

#### Mapper
> Long option: `--mapper MAPPER`<br>
> Short option: `-m`

Mapper to use for mapping. Default value is 'bowtie2' 

#### Multifasta
> Long option: `--multifasta `<br>
> Short option: `-M`

Per default, multiple sequences of a given reference FASTA file are considered to be different genetic segments (e.g., different chromosomes). In case --multi-fasta is used, the pipeline splits the multi-FASTA file into single FASTA files, and subsequently calls RefFinder (developed by Dr. Stephan Fuchs) to automatically select the best reference.

#### Rename
> Long option: `--rename RENAME`<br>
> Short option: `-R`

Tab-separated file with two columns: `<old sample name> <new sample name>`. QCumber replaces the old filename with the new one. If it does not find unique replacements, it will skip renaming for this sample.


#### Sequencer platform
> Long option: `--platform` <br>
> Short option: `-p` <br> 
> options: {'ILLUMINA', 'SOLID', 'LS454', 'HELICOS', 'PACBIO'}

Define sequencer platform . Default: ILLUMINA

#### QC SAV
> Long option: `--qc_sav QC_SAV`

This option takes all arguments from **_RunInfo.xml_**  and **_RunParameter.xml_** and converts it into a human readable table. Furthermore, plots were generated equivalent to SAV section "Data by Cycle" for FWHM, intensity and %base as well as for section "Data by Lane" for prephasing, phasing and cluster density. Both tables and plots can be found in **QCResults/batch_report.html** under the section "Sequencer Information". Additionally, a report **QCResults/SAV.pdf** for SAV will be generated.


#### QC adapter
> Long option: `--qc_adapter QC_ADAPTER` <br>
> options: {TruSeq2-PE, TruSeq2-SE, TruSeq3-PE, TruSeq3-SE, TruSeq3-PE-2, NexteraPE-PE}

Define adapter sequence for Trimmomatic. Suggested adapter sequences are provided for TruSeq2 (as used in GAII machines) and TruSeq3 (as used by HiSeq and MiSeq machines), for both single-end and paired-end mode (check Trimmomatic manual). If not set, all adapters are used for trimming.


#### QC trimbetter
> Long option: `--qc_trimBetter `

Optimize trimming parameter using 'Per sequence base content' from fastqc

#### QC read minlen
> Long option: `--qc_minlen QC_MINLEN`

Minlen parameter for Trimmomatic. Drops read short than minlen. Default: 50


#### QC nokraken
> Long option: `--qc_nokraken `

Skip Kraken step

#### Mapping options
> Long option: `--mapping_options MAPPING_OPTIONS`

Additional options for the mapper

#### Mapping unmapped
> Long option: `--mapping_unmapped MAPPING_UNMAPPED`

Write unmapped reads to given file. Read name will be automatically added

#### Mapping reffinder options
> Long option: `--mapping_reffinder_options MAPPING_REFFINDER_OPTIONS`

Additional options for RefFinder.

#### VC annotate all
> Long option: `--vc_annotate_all `

Report also variants in non-coding regions. Default: only variants in the coding regions will be reported.

#### VC decimal sep
> Long option: `--vc_decimal_sep VC_DECIMAL_SEP`

Set the default decimal separator, e.g. 10.323 or 10,323

#### VC ploidy
> Long option: `--vc_ploidy VC_PLOIDY`

Sample ploidy for GATK. Default: 1

#### VC ambiguity cutoff
> Long option: `--vc_ambiguity_cutoff VC_AMBIGUITY_CUTOFF`

This option is used to define the minimum percent of coverage difference of a variant position that is to be treated as ambiguous in the consensus sequence. Default: 5%. For example, --ambiguity_cutoff 5 will encode the genotype A/T as W if the lower coverage of the reference allele A and the alternative allele T represents at least 5 % of the total coverage.

#### VC majority
> Long option: `--vc_majority `

Use majority vote for called alleles in the consensus sequence generation. For example, if the called genotype is A/T and the reference allele A has a higher coverage than the alternative allele T, then A is used for the consensus sequence. If both alleles have the same coverage, ambiguous encoding is used.


## Output

By default, the pipeline generates the following files in the output folder:

* qc
	* [QCumber output](https://gitlab.com/RKIBioinformaticsPipelines/QCumber/tree/version_2)
* mapping 
	* [Mapping output](https://gitlab.com/RKIBioinformaticsPipelines/Mapping/)
* vc
	* [Variant calling output](https://gitlab.com/RKIBioinformaticsPipelines/VariantCalling)


## Changelog


## License

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License, version 3
as published by the Free Software Foundation.

This program is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this program.  If not, see
http://www.gnu.org/licenses/.